export PATH="${HOME}/bin:${HOME}/.cargo/bin:${PATH}"
export EDITOR='vi'
export HISTFILE="${HOME}/.sh_history"
export LC_CTYPE="en_US.UTF-8"
export XDG_CONFIG_HOME=${HOME}/.config/

alias ls='ls -AFh'
alias vi="${EDITOR}"
alias apgrade='doas apk update && doas apk upgrade'

PS1='$(echo "${PWD##*/}") $(echo -e "\e[1;34m")λ$(echo -e "\e[0m") '

# if [ -z $DISPLAY ] && [ $(tty) = /dev/ttyC0 ]; then
#    startx
# fi
